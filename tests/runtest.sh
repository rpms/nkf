#! /bin/sh

set -e

q="テスト"
res=$(echo $q | nkf -W -e | nkf -w -E)
ret=1

if [ x$q = x$res ]; then
  ret=0
  mes="PASS"
else
  ret=1
  mes="FAIL"
fi

echo $mes
exit $ret
